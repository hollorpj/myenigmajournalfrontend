import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityBackdropComponent } from './city-backdrop.component';

describe('CityBackdropComponent', () => {
  let component: CityBackdropComponent;
  let fixture: ComponentFixture<CityBackdropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityBackdropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityBackdropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
