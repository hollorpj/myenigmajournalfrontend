import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectoryBookComponent } from './directory-book.component';

describe('DirectoryBookComponent', () => {
  let component: DirectoryBookComponent;
  let fixture: ComponentFixture<DirectoryBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectoryBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectoryBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
