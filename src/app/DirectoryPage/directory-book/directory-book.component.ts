import { Component, OnInit, Input } from '@angular/core';
import { ConfigService } from '../../services/configservice.service';

@Component({
  selector: 'app-directory-book',
  templateUrl: './directory-book.component.html',
  styleUrls: ['./directory-book.component.css']
})
export class DirectoryBookComponent implements OnInit {
 
  @Input()
  top;
  
  @Input()
  left;
  
  @Input()
  height;
  
  @Input()
  width;
  
  styles;

  constructor(private configService : ConfigService) { }

  ngOnInit() {
   this.styles = {
   	'top' : this.top,
   	'left' : this.left,
   	'height' : this.height,
   	'width' : this.width
   }
  }
  
  logout(){
    this.configService.logout();
  }

}
