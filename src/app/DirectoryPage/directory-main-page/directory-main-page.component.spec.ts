import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectoryMainPageComponent } from './directory-main-page.component';

describe('DirectoryMainPageComponent', () => {
  let component: DirectoryMainPageComponent;
  let fixture: ComponentFixture<DirectoryMainPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectoryMainPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectoryMainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
