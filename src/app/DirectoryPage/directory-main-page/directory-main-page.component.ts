import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directory-main-page',
  templateUrl: './directory-main-page.component.html',
  styleUrls: ['./directory-main-page.component.css']
})
export class DirectoryMainPageComponent implements OnInit {

  bookWidth = '50%';
  bookHeight = '90%';
  bookTop = '2%';
  bookLeft = '25%';

  constructor() { }

  ngOnInit() {
  }

}
