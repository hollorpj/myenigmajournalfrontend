import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../services/configservice.service';
import { PageFlipperService } from '../../services/page-flipper.service';
import { UserDataOrchestratorService } from '../../services/user-data-orchestrator.service';
import { CryptographyService } from '../../services/cryptography.service';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-chapters',
  templateUrl: './list-chapters.component.html',
  styleUrls: ['./list-chapters.component.css']
})
export class ListChaptersComponent implements OnInit {

  title : string;
  
  chapters;
  
  showList = true;
  showNewEntry = false;
  showKeyUploader = false;
  fileReader;
  
  textColor;

  constructor(private configService : ConfigService, private http : Http, private router : Router, private pageManager : PageFlipperService, private userDataOrchestrator : UserDataOrchestratorService, private cryptography : CryptographyService) { 
    this.title = this.userDataOrchestrator.getCurrentUser() + "'s Journal";
    this.fileReader = new FileReader();
    
  }

  ngOnInit() {
    let url = this.configService.fetchJournalEntryNamesUrl;
    let requestData = {
    	'cookie' : this.configService.getCookie()
    };
    
    let headers = new Headers();
    headers.append("content-type", "application/json");
    
    this.http.post(url, requestData, headers).subscribe(response => {
     this.chapters = JSON.parse(response['_body']);
    },
    err => {
      if (err['status'] == 403) {
        alert('Your session has expired. Please login again.');
        this.router.navigateByUrl('');
      }
    });
  
  }
  
  bringUpNewEntryMenu() {
  	this.showList = false;
  	this.showNewEntry = true;
  }
  
  requestKeyIfNotCached(entryName) {
  	this.showKeyUploader = true;
  }
  
  mouseEnter(name) {
	name.style.color = '#3b5998';
	name.style.fontWeight = 'bold';
  }

  mouseLeave(name) {
    name.style.color = '';
    name.style.fontWeight = 'normal';
  }
  
  loadEntry(entryName) {
  	let url = this.configService.fetchJournalEntryUrl;
    let requestData = {
    	'cookie' : this.configService.getCookie(),
    	'id' : entryName
    };
    
    let headers = new Headers();
    headers.append("content-type", "application/json");
  
  	this.http.post(url, requestData, headers).subscribe(response => {
    	let entryText = JSON.parse(response['_body']);
    	this.pageManager.setEditorData(entryText);
    	this.pageManager.setDocumentName(entryName);
    	
    	this.router.navigateByUrl('/editor/' + entryName);
  	});
  }


}
