import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeentryDefinerComponent } from './neentry-definer.component';

describe('NeentryDefinerComponent', () => {
  let component: NeentryDefinerComponent;
  let fixture: ComponentFixture<NeentryDefinerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeentryDefinerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeentryDefinerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
