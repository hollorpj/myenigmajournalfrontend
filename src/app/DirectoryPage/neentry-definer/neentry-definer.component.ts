import { Component, OnInit } from '@angular/core';
import { PageFlipperService } from '../../services/page-flipper.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-neentry-definer',
  templateUrl: './neentry-definer.component.html',
  styleUrls: ['./neentry-definer.component.css']
})
export class NeentryDefinerComponent implements OnInit {

  constructor(private pageHandlerService : PageFlipperService, private router : Router) { }

  ngOnInit() {
  }
  
  createNewPage(name) {
  	this.pageHandlerService.setDocumentName(name.value);
  	this.pageHandlerService.setEditorData(['', '']);
  	this.router.navigateByUrl('/editor/' + name);
  }
  
  

}
