import { ConfigService } from '../../services/configservice.service';
import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { LoginOrchestratorService } from '../../services/login-orchestrator.service';
import { UserDataOrchestratorService } from '../../services/user-data-orchestrator.service';

@Component({
  selector: 'app-login-box',
  templateUrl: './login-box.component.html',
  styleUrls: ['./login-box.component.css']
})
export class LoginBoxComponent implements OnInit {

  errorMessage;
  errorLoginStyling = {};

  constructor(private http : Http, private configService : ConfigService, private router : Router, private loginOrchestrator : LoginOrchestratorService, private userDataOrchestrator : UserDataOrchestratorService) { }

  ngOnInit() {
    this.loginOrchestrator.registrationHorn.subscribe(username => {
    	this.errorMessage = username + ' has been registered successfully!';
    });
  }

  auth(username, password, loginBox) {
    let usernameVal = username.value;
    let passwordVal = password.value;
    
    let loginObject = {
      'userId' : usernameVal,
      'password' : passwordVal
    };
    
    let headers = new Headers();
    headers.append("content-type", "application/json");
    headers.append("Access-Control-Allow-Origin", "*");
    
    let url = this.configService.getLoginUrl();
    this.errorMessage = 'Logging in... Please Wait';
    this.errorLoginStyling = {
      	'background-color': 'rgba(0, 255, 0, .15)'
      };
    
    this.http.post(url, loginObject, headers).subscribe(response => {
      this.errorLoginStyling = {};
      this.errorMessage = '';
    
      let cookie = response['_body'];
      document.cookie = cookie;
      this.userDataOrchestrator.setCurrentUser(usernameVal);
      
      let curPosition = loginBox.offsetTop;
      let targetPosition = -1 * loginBox.offsetHeight;
      this.flyAwayLoginBox(loginBox, curPosition, targetPosition);
    },
    err => {
      if (err['status'] === 403) {
		this.errorMessage = "Incorrect username or password";      
      } else {
        this.errorMessage = "System error. Please try again later.";
      }
      
      this.errorLoginStyling = {
      	'background-color': 'rgba(255, 0, 0, .15)'
      };
    });
  }
  
  loadRegisterPage(loginBox) {
    let position = loginBox.offsetLeft;
    let width = loginBox.offsetWidth;
    let moveElementUntil = -1 * width;

    this.unloadLoginPageLoadRegisterPage(loginBox, position, position, moveElementUntil);
  }
  
  unloadLoginPageLoadRegisterPage(elementToMove, initialPosition, curPosition, targetPosition) {
    
      return new Promise(resolve => {
        setTimeout(() => {
          curPosition = curPosition - 8;
          elementToMove.style.left = curPosition + 'px';  
          if (curPosition > targetPosition) {
            this.unloadLoginPageLoadRegisterPage(elementToMove, initialPosition, curPosition, targetPosition);
          } else {
           let registrationBox = document.getElementById("registerContainer");
           let registrationBoxPosition = registrationBox.offsetLeft;
           registrationBox.style.visibility = 'visible';
           this.slideInRegisterPage(registrationBox, registrationBoxPosition, initialPosition); 
          }       
        }, .005);
      })
  }
  
 
  slideInRegisterPage(elementToMove, curPosition, targetPosition) {
    return new Promise(resolve => {
      setTimeout(() => {
       curPosition = curPosition - 8;
       elementToMove.style.left = curPosition + 'px';
       if (curPosition > targetPosition) {
         this.slideInRegisterPage(elementToMove, curPosition, targetPosition);
       } else if (curPosition < targetPosition) {
         elementToMove.style.left = targetPosition + 'px';  
       }
    }, .005);
      });
  
  }
  
  flyAwayLoginBox(loginBox, curPosition, targetPosition) {
    return new Promise(resolve => {
      setTimeout(() => {
       curPosition = curPosition - 8;
       loginBox.style.top = curPosition + 'px';
       if (curPosition > targetPosition) {
         this.flyAwayLoginBox(loginBox, curPosition, targetPosition);
       } else {
         this.router.navigateByUrl('/home');
       }
      }, .005);
    });
  }
 
  
  
  

  
}
