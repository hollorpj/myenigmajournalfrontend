import { ConfigService } from '../../services/configservice.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { LoginOrchestratorService } from '../../services/login-orchestrator.service';

@Component({
  selector: 'app-register-box',
  templateUrl: './register-box.component.html',
  styleUrls: ['./register-box.component.css']
})
export class RegisterBoxComponent implements OnInit {

  @ViewChild('registerContainer')
  private registerContainer;
  
  infoMessage;
  errorMessage;

  constructor(private configService : ConfigService, private http : Http, private loginOrchestrator : LoginOrchestratorService) { }

  ngOnInit() {
  }

  registerAccount(username, password, passwordConfirm, email) {
    
      let usernameVal = username.value;
      let passwordVal = password.value;
      let passwordConfirmVal = passwordConfirm.value;
      let emailVal = email.value;
    
      if (this.isInvalid(usernameVal, passwordVal, passwordConfirmVal, emailVal)) {
        return;
      }
      this.errorMessage = undefined;
      this.infoMessage = 'Processing Registration...';
    
	  let registerUrl = this.configService.getCreateUserUrl();
	  let requestBody = {
	    'userId' : usernameVal,
	    'password' : passwordVal,
	    'email' : emailVal
	  }
	
	  let headers = new Headers();
	  headers.append("content-type", "application/json");
	  this.http.post(registerUrl, requestBody, headers).subscribe(response => {
	    this.infoMessage = undefined;
	    this.handleSuccess(usernameVal);
	  },
	  err => {
	    this.infoMessage = undefined;
	    let status = err['status'];
	    if (status === 409) {
	      this.errorMessage = "That username or email has already been taken."
	    } else {
	      this.errorMessage = "System error. Please try again later.";
	    }
	    
	  });
  }
  
  
  handleSuccess(username) {
    this.loginOrchestrator.userRegisteredSuccessfully(username);
    this.loadLoginPage();
  }
  
  isInvalid(username, password, passwordConfirm, email) {
     if (username === '') {
       this.errorMessage = "Please enter a username";
       return true;
     }
     if (password == '') {
       this.errorMessage = "Please enter a password";
       return true;
     }
     if (passwordConfirm == '') {
       this.errorMessage = "Please confirm your password";
       return true;
     }
     if (email == '') {
       this.errorMessage = "Please enter an e-mail";
       return true;
     }
     
     if (password != passwordConfirm) {
        this.errorMessage = "Passwords do not match";
     	return true;
     }
  }
  
  // animation
  
  loadLoginPage() {
    let registerBox = document.getElementById("registerContainer");
    
    let position = registerBox.offsetLeft;
    let width = registerBox.offsetWidth;
    let moveElementUntil = -1 * width;

    this.unloadRegisterPageLoadLoginPage(registerBox, position, position, moveElementUntil);
  }
  
  unloadRegisterPageLoadLoginPage(elementToMove, initialPosition, curPosition, targetPosition) {
    
      return new Promise(resolve => {
        setTimeout(() => {
          curPosition = curPosition - 8;
          elementToMove.style.left = curPosition + 'px';  
          if (curPosition > targetPosition) {
            this.unloadRegisterPageLoadLoginPage(elementToMove, initialPosition, curPosition, targetPosition);
          } else {
           let loginBox = document.getElementById("loginContainer");
           let loginBoxPosition = window.screen.availWidth;
           loginBox.style.visibility = 'visible';
           this.slideInLoginPage(loginBox, loginBoxPosition, initialPosition); 
          }       
        }, .005);
      })
  }
  
 
  slideInLoginPage(elementToMove, curPosition, targetPosition) {
    return new Promise(resolve => {
      setTimeout(() => {
       curPosition = curPosition - 8;
       elementToMove.style.left = curPosition + 'px';
       if (curPosition > targetPosition) {
         this.slideInLoginPage(elementToMove, curPosition, targetPosition);
       } else if (curPosition < targetPosition) {
         elementToMove.style.left = targetPosition + 'px';  
       }
    }, .005);
      });
  
  }
  
}
