import { Component, OnInit, Input } from '@angular/core';
import { PageFlipperService } from '../../services/page-flipper.service';

@Component({
  selector: 'app-back-button',
  templateUrl: './back-button.component.html',
  styleUrls: ['./back-button.component.css']
})
export class BackButtonComponent implements OnInit {

  @Input()
  private top;
  
  @Input()
  private left;
  
  @Input()
  private height;
  
  @Input()
  private width;
  
  styling;
  
  enabled : boolean;

  constructor(private pageFlipper : PageFlipperService ) { 
  	
  }

  ngOnInit() {
  	this.styling = {
  		'top' : this.top,
  		'left' : this.left,
  		'width' : this.width,
  		'height' : this.height
  	}
  	this.pageFlipper.isBackwardButtonEnabledObservable.subscribe(enablement => this.enabled = enablement);
  }
  
  moveBackward() {
   this.pageFlipper.moveNPages(-2);
  }

}
