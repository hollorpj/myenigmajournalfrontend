import { Component, OnInit, Input, ViewChild, HostListener } from '@angular/core';

import { PageFlipperService } from '../../services/page-flipper.service';

@Component({
  selector: 'app-editor-pane',
  templateUrl: './editor-pane.component.html',
  styleUrls: ['./editor-pane.component.css']
})
export class EditorPaneComponent implements OnInit {

  @ViewChild('editor') editor;

  @Input() width;
  @Input() height;
  @Input() top;
  @Input() left;
  @Input() pageNumber;
  
  styles;
  
  private editorContentJson;
  
  constructor(private pageFlipper : PageFlipperService ) {
  	
  }
   
  keyShortcutHandler(keypress) {
    let key = keypress.key;
	this.pageFlipper.markCurrent(false);
    
    if (key === 'Tab') {
    	keypress.preventDefault();
    	let elem = this.editor.nativeElement;
    	let caretPosition = elem.selectionStart;
    	let text = elem.value;
    	let newText = text.substring(0, caretPosition) + '\t';
    	if (caretPosition != text.length) {
    		newText = newText + text.substring(caretPosition);
    	}
    	elem.value = newText;
    	elem.selectionStart = caretPosition + 1;
    	elem.selectionEnd = elem.selectionStart;
	}
	
  } 
  
  handlePageFlip(pagesToFlip) {
    if (pagesToFlip != 0) {
      this.pageFlipper.storePage(this.pageNumber, this.editor.nativeElement.value);
    }
  
    
  	this.pageNumber += pagesToFlip;
  
  	let maximumPages = this.pageFlipper.getNumPages();
  	if ((this.pageNumber) === maximumPages - 1) {
  		this.editor.nativeElement.value = this.pageFlipper.getPage(this.pageNumber);
  		this.pageFlipper.setAddNewButtonState(true);
  		this.pageFlipper.setForwardButtonState(false);
  		return;
  	} else {
  		this.pageFlipper.setForwardButtonState(true);
  		this.pageFlipper.setAddNewButtonState(false);
  	}
  	
  	if (this.pageNumber == 0 || (this.pageNumber - 1) == 0) {
  	    this.pageFlipper.setBackButtonState(false);
  	} else {
  	    this.pageFlipper.setBackButtonState(true);
  	}
  	
  	
  	this.editor.nativeElement.value = this.pageFlipper.getPage(this.pageNumber);
  }
  
  private pageFlipSubscription;
  private savePageSubscription;
  
  ngOnInit() {
    this.styles = {
      'width': this.width,
      'height': this.height,
      'max-height': this.height,
      'top' : this.top,
      'left' : this.left
    }
    
    this.pageFlipSubscription = this.pageFlipper.pageWidow.subscribe(numberToChange => {
		this.handlePageFlip(numberToChange);
	});
	
	this.savePageSubscription = this.pageFlipper.savePage.subscribe(nan => {
		this.pageFlipper.storePage(this.pageNumber, this.editor.nativeElement.value);
	});
	
	this.pageFlipper.setBackButtonState(false);
	
  }
  
  ngOnDestroy() {
    this.pageFlipSubscription.unsubscribe();
    this.savePageSubscription.unsubscribe();
  }
 

}
