import { Component, OnInit, Input } from '@angular/core';
import { PageFlipperService } from '../../services/page-flipper.service';

@Component({
  selector: 'app-forward-button',
  templateUrl: './forward-button.component.html',
  styleUrls: ['./forward-button.component.css']
})
export class ForwardButtonComponent implements OnInit {

  @Input()
  private top;
  
  @Input()
  private left;
  
  @Input()
  private height;
  
  @Input()
  private width;
  
  styling;

  enabled : boolean;

  constructor(private pageFlipper : PageFlipperService) { }

  ngOnInit() {
  	this.styling = {
  		'top' : this.top,
  		'left' : this.left,
  		'width' : this.width,
  		'height' : this.height
  	}
  	this.pageFlipper.isForwardButtonEnabledObservable.subscribe(enablement => this.enabled = enablement);
  }
  
  moveForward() {
   this.pageFlipper.moveNPages(2);
  }

}
