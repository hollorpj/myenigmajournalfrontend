import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPageButtonComponent } from './new-page-button.component';

describe('NewPageButtonComponent', () => {
  let component: NewPageButtonComponent;
  let fixture: ComponentFixture<NewPageButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPageButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPageButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
