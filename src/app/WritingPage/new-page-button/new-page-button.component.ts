import { Component, OnInit, Input } from '@angular/core';
import { PageFlipperService } from '../../services/page-flipper.service';

@Component({
  selector: 'app-new-page-button',
  templateUrl: './new-page-button.component.html',
  styleUrls: ['./new-page-button.component.css']
})
export class NewPageButtonComponent implements OnInit {
 
  @Input()
  private top;
  
  @Input()
  private left;
  
  @Input()
  private height;
  
  @Input()
  private width;
  
  private styling;

  enabled : boolean;

  constructor(private pageFlipper : PageFlipperService) { }

  ngOnInit() {
  	this.styling = {
  		'top' : this.top,
  		'left' : this.left,
  		'width' : this.width,
  		'height' : this.height
  	}
  	this.pageFlipper.isNewButtonEnabledObservable.subscribe(enablement => this.enabled = enablement);
  }
  
  newPage() {
   this.pageFlipper.createPage();
  }

}
