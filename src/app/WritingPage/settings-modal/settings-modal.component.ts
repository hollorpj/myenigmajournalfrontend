import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsEditorHustlerService } from '../../services/settings-editor-hustler.service';
import { PageFlipperService } from '../../services/page-flipper.service';

@Component({
  selector: 'app-settings-modal',
  templateUrl: './settings-modal.component.html',
  styleUrls: ['./settings-modal.component.css']
})
export class SettingsModalComponent implements OnInit {

  mode : string;
  text : string;

  constructor(private canvasHustler : SettingsEditorHustlerService, private pageManagementService : PageFlipperService, private router : Router) { 
    this.mode = 'main-menu';
  }

  ngOnInit() {
    
  }
  
  closeSettings() {
  	this.canvasHustler.closeTheSettingsWindow();
  }
  
  saveEntryAndGoHome() {
    this.pageManagementService.saveEntry();
    this.canvasHustler.closeTheSettingsWindow();
    this.router.navigateByUrl('/home');
  }
  
  saveEntry() {
    this.pageManagementService.saveEntry().subscribe(response => {
      if (response['status'] === 200) {
        this.text = "Successfully Saved";
      } else {
        this.text = "Cannot save at this time. Please backup your changes locally.";
      }
    
    console.log(response);
    });
  }
  
  warnBeforeNavigatingHome() {
    if (this.pageManagementService.isCurrent()) {
      this.navigateHome();
    } else {
      this.mode = 'should-save-before-leaving';
    }
    
  }
  
  navigateHome() {
  	this.canvasHustler.closeTheSettingsWindow();
    this.router.navigateByUrl('/home');
  }

}
