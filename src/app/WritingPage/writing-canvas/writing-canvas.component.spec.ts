import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WritingCanvasComponent } from './writing-canvas.component';

describe('WritingCanvasComponent', () => {
  let component: WritingCanvasComponent;
  let fixture: ComponentFixture<WritingCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WritingCanvasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WritingCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
