import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { SettingsEditorHustlerService } from '../../services/settings-editor-hustler.service';
import { PageFlipperService } from '../../services/page-flipper.service';
import { Observable } from 'rxjs';
 
@Component({
  selector: 'app-writing-canvas',
  templateUrl: './writing-canvas.component.html',
  styleUrls: ['./writing-canvas.component.css']
})
export class WritingCanvasComponent implements OnInit {

  @ViewChild('writingCanvas')
  private thisComponent;
  
  width;
  height;
  top;
  left;
  
  navWidth;
  navHeight;
  navTop;
  backLeft;
  fwdLeft;
  
  secondLeft;
  
  hideThePageCss;
  showSettings = false;
  
  starterPageLeft;
  starterPageRight;
  
  
  constructor(private settingsPageHustler : SettingsEditorHustlerService, private pageFlipper : PageFlipperService) {
    let firstRun = true; 
    this.settingsPageHustler.isWindowOpen.subscribe(isOpen => {
    	if (isOpen) {
    		this.fadeOutPage(0, 10, .32);
    	} else {
    		if (!firstRun) {
    			this.showSettings = false;
    	    	this.fadeInPage(10, 0, .32);
    		}
    	}
    	
    });
    
    firstRun = false;
   }
   
  ngOnInit() {
    this.width = '47.5%';
    this.height = '99%';
    this.top = '0%';
    this.left = '0%';
    this.secondLeft = '52%';
    
    this.navWidth = '10%';
    this.navHeight = '80%';
    this.navTop = '10%';
    this.backLeft = '0%';
    this.fwdLeft = '90%';

	this.starterPageLeft = 0;
	this.starterPageRight = 1;
	
	this.showSettings = false;
	this.pageFlipper.moveNPages(0);    
  }
  
  clickOnSettings() {
    this.settingsPageHustler.openTheSettingsWindow();
  	
  }
  
  fadeOutPage(curFadeLevel, targetFadeLevel, stepSize) {
  	return new Promise(resolve => {
  		setTimeout(() => {
  		  curFadeLevel += stepSize;
		  this.hideThePageCss = {
  	        'filter': 'blur(' + curFadeLevel + 'px)',
			'pointer-events': 'none'
  		  }
  		  
  		  if (curFadeLevel < targetFadeLevel) {
  		    this.fadeOutPage(curFadeLevel, targetFadeLevel, stepSize);
  		  } else {
  		    this.pageFlipper.cacheCurrentPage();
  		  	this.showSettings = true;
  		  }
  		}, 2)
  	});
  }
  
  fadeInPage(curFadeLevel, targetFadeLevel, stepSize) {
  	return new Promise(resolve => {
  		setTimeout(() => {
  		  curFadeLevel -= stepSize;
		  this.hideThePageCss = {
  	        'filter': 'blur(' + curFadeLevel + 'px)',
			'pointer-events': 'all'
  		  }
  		  
  		  if (curFadeLevel > targetFadeLevel) {
  		    this.fadeInPage(curFadeLevel, targetFadeLevel, stepSize);
  		  } else {
  		  	this.hideThePageCss = {
  	          'filter': 'blur(' + targetFadeLevel + 'px)',
		  	  'pointer-events': 'all'
  		    }  
  		    
  		    this.showSettings = false;
  		  }
  		}, 2)
  	});
  }	
  
  @HostListener('window:beforeunload')
  canDeactivate(): Observable<boolean> | boolean {
    return false;
  }
  


}
