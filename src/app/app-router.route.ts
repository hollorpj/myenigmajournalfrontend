import { LoginPageComponent } from './LoginPage/login-page/login-page.component';
import { SettingsModalComponent } from './WritingPage/settings-modal/settings-modal.component';
import { WritingCanvasComponent } from './WritingPage/writing-canvas/writing-canvas.component';
import { DirectoryMainPageComponent } from './DirectoryPage/directory-main-page/directory-main-page.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: '',
        component: LoginPageComponent
      },
      {
        path: 'editor/:entryName',
        component: WritingCanvasComponent
      },
      {
        path: 'home',
        component: DirectoryMainPageComponent        
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})

export class AppRouterModule {
  
}
