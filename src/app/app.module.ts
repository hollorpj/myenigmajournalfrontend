import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CreateUserComponent } from './pilot/login/create-user/create-user.component';
import { ConfigService } from './services/configservice.service';
import { PageFlipperService } from './services/page-flipper.service';
import { SettingsEditorHustlerService } from './services/settings-editor-hustler.service';
import { HttpModule } from '@angular/http';
import { SigninComponent } from './pilot/login/signin/signin.component';
import { VerificationService } from './services/verification.service';
import { LoginOrchestratorService} from './services/login-orchestrator.service';
import { UserDataOrchestratorService } from './services/user-data-orchestrator.service';
import { CryptographyService } from './services/cryptography.service';
import { LoginPageComponent } from './LoginPage/login-page/login-page.component';
import { LoginBoxComponent } from './LoginPage/login-box/login-box.component';
import { AppRouterModule } from './app-router.route';
import { RegisterBoxComponent } from './LoginPage/register-box/register-box.component';
import { WritingCanvasComponent } from './WritingPage/writing-canvas/writing-canvas.component';
import { EditorPaneComponent } from './WritingPage/editor-pane/editor-pane.component';
import { CityBackdropComponent } from './Backdrops/city-backdrop/city-backdrop.component';
import { BackButtonComponent } from './WritingPage/back-button/back-button.component';
import { ForwardButtonComponent } from './WritingPage/forward-button/forward-button.component';
import { SettingsModalComponent } from './WritingPage/settings-modal/settings-modal.component';
import { NewPageButtonComponent } from './WritingPage/new-page-button/new-page-button.component';
import { DirectoryMainPageComponent } from './DirectoryPage/directory-main-page/directory-main-page.component';
import { DirectoryBookComponent } from './DirectoryPage/directory-book/directory-book.component';
import { ListChaptersComponent } from './DirectoryPage/list-chapters/list-chapters.component';
import { NeentryDefinerComponent } from './DirectoryPage/neentry-definer/neentry-definer.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateUserComponent,
    SigninComponent,
    LoginPageComponent,
    LoginBoxComponent,
    RegisterBoxComponent,
    WritingCanvasComponent,
    EditorPaneComponent,
    CityBackdropComponent,
    BackButtonComponent,
    ForwardButtonComponent,
    SettingsModalComponent,
    NewPageButtonComponent,
    DirectoryMainPageComponent,
    DirectoryBookComponent,
    ListChaptersComponent,
    NeentryDefinerComponent,
  ],
  imports: [
    BrowserModule,
    AppRouterModule,
    HttpModule
  ],
  providers: [ConfigService, VerificationService, SettingsEditorHustlerService, PageFlipperService, LoginOrchestratorService, UserDataOrchestratorService, CryptographyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
