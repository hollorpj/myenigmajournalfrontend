import { ConfigService } from '../../../services/configservice.service';
import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  constructor(private http : Http, private configService : ConfigService) { }

  ngOnInit() {
  }
  
  createUser(usernameCreate, passwordCreate, passwordConfirm) {
    let formData = {
      'userId' : usernameCreate.value,
      'password' : passwordCreate.value
    }
    
    let headers = new Headers();
    headers.append('content-type', 'application/json');
    
    this.http.post(this.configService.getCreateUserUrl(), formData, headers).subscribe(response => {
      alert("don")
    });
  }

}
