import { ConfigService } from '../../../services/configservice.service';
import { VerificationService } from '../../../services/verification.service';
import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(private http : Http, private configService : ConfigService, private verificationService : VerificationService) { }

  ngOnInit() {
  }
  
  login(usernameSignIn, passwordSignIn) {
    let loginInfo = {
      'userId' : usernameSignIn.value,
      'password' : passwordSignIn.value
    }
    
    let header = new Headers();
    header.append('content-type', 'application/json');
    
    this.http.post(this.configService.getLoginUrl(), loginInfo, header).subscribe(response => {
      document.cookie = response['_body'];
    });
    
  }
  
  isMyCookieOk() {
    this.verificationService.isCookieStillAlive();
  }

}
