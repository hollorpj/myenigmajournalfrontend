import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class ConfigService {

  private baseUrl : string;
  private createUserUrl : string;
  private loginUrl : string;
  private cookieVerificationUrl : string;
  fetchJournalEntryNamesUrl : string;
  fetchJournalEntryUrl : string;
  saveEntryUrl : string;
  
  cookieLifespan : number;
  
  private cookieKey = 'cookie';
  
  
  constructor(private router : Router) {
    //this.baseUrl = "http://localhost:8080";
    this.baseUrl = "https://my-secret-journal-backend.herokuapp.com";
    this.createUserUrl = this.baseUrl + "/createUser";
    this.loginUrl = this.baseUrl + "/login";
    this.cookieVerificationUrl = this.baseUrl + "/verifyCookie";
    this.fetchJournalEntryNamesUrl = this.baseUrl + "/fetchNames";
    this.fetchJournalEntryUrl = this.baseUrl + "/fetchEntry";
    this.saveEntryUrl = this.baseUrl + "/saveEntry";
    
    this.cookieLifespan = 60000; 
   }
  
  getCreateUserUrl() {
    return this.createUserUrl;
  }
  
  getLoginUrl() {
    return this.loginUrl;
  }
  
  getCookieVerificationUrl() {
    return this.cookieVerificationUrl;
  }
  
  getCookie() {
    let rawCookie = document.cookie;
    let components = rawCookie.split(';');
    
    
    for (let i = 0; i < components.length; i++) {
      var name = components[i].split("=")[0].trim();
    
      if (name === this.cookieKey) {
        return components[i].split("=")[1].trim();
      }
    }
    
    return null;
  }
  
  logout() {
    document.cookie = this.cookieKey + "=;MaxAge=0";
    this.router.navigateByUrl("/");
  }

}
