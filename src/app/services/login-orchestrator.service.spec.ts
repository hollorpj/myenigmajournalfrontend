import { TestBed, inject } from '@angular/core/testing';

import { LoginOrchestratorService } from './login-orchestrator.service';

describe('LoginOrchestratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginOrchestratorService]
    });
  });

  it('should be created', inject([LoginOrchestratorService], (service: LoginOrchestratorService) => {
    expect(service).toBeTruthy();
  }));
});
