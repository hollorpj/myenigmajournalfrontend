import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/Rx';
import { Subject } from 'rxjs';

@Injectable()
export class LoginOrchestratorService {

  registrationHorn = new Subject();

  constructor() { }

  userRegisteredSuccessfully(username) {
  alert(username);
  	this.registrationHorn.next(username);
  } 

}
