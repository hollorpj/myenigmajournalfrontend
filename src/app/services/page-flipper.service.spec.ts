import { TestBed, inject } from '@angular/core/testing';

import { PageFlipperService } from './page-flipper.service';

describe('PageFlipperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PageFlipperService]
    });
  });

  it('should be created', inject([PageFlipperService], (service: PageFlipperService) => {
    expect(service).toBeTruthy();
  }));
});
