import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import {BehaviorSubject} from 'rxjs/Rx';
import { ConfigService } from '../services/configservice.service';
import { UserDataOrchestratorService } from '../services/user-data-orchestrator.service';
import { Subject } from 'rxjs';

@Injectable()
export class PageFlipperService {

  private editorContentJson;

  private pageFlipper = new BehaviorSubject<number>(0);
  pageWidow = this.pageFlipper.asObservable();
  
  private isForwardButtonEnabled = new BehaviorSubject<boolean>(true);
  isForwardButtonEnabledObservable = this.isForwardButtonEnabled.asObservable();
  
  private isBackwardButtonEnabled = new BehaviorSubject<boolean>(false);
  isBackwardButtonEnabledObservable = this.isBackwardButtonEnabled.asObservable();
  
  private isNewButtonEnabled = new BehaviorSubject<boolean>(false);
  isNewButtonEnabledObservable = this.isNewButtonEnabled.asObservable();
  
  savePage = new Subject();
  
  private documentName;
  private isDocumentCurrent = true;

  constructor(private http : Http, private configService : ConfigService, private userDataOrchestrator : UserDataOrchestratorService) { 
  }
  
  ngOnInit() {
  	
  }
  
  moveNPages(number) {
  	this.pageFlipper.next(number);
  }
  
  setBackButtonState(state) {
  	this.isBackwardButtonEnabled.next(state);
  }
  
  setForwardButtonState(state) {
  	this.isForwardButtonEnabled.next(state);
  }
  
  setAddNewButtonState(state) {
  	this.isNewButtonEnabled.next(state);
  }
  
  createPage() {
   this.editorContentJson.push('');
   this.editorContentJson.push('');
   
   this.moveNPages(2);
  } 
  
  getPage(pageNum) {
  	return this.editorContentJson[pageNum];
  }
  
  getNumPages() {
    return this.editorContentJson.length;
  }
  
  storePage(pageNum, pageContent) {
  	this.editorContentJson[pageNum] = pageContent;
  }
  
  setDocumentName(name) {
  	this.documentName = name;
  }
  
  setEditorData(data) {
  	this.editorContentJson = data;
  }
  
  cacheCurrentPage() {
  	this.savePage.next('rand');
  }
  
  saveEntry() {
    let cookie = this.configService.getCookie();
    let username = this.userDataOrchestrator.getCurrentUser();
    let name = this.documentName;
    let content = this.editorContentJson;
    
    
    let saveData = {
    	'cookie' : cookie,
    	'entryId' : this.documentName,
    	'content' : this.editorContentJson
    }
    
    let headers = new Headers();
    headers.append('content-type', 'application/json');
    
    let url = this.configService.saveEntryUrl;
    this.markCurrent(true);
    
    return this.http.post(url, saveData, headers);
  }
  
  markCurrent(state) {
    this.isDocumentCurrent = state;
  }
  
  isCurrent() {
  	return this.isDocumentCurrent;
  }
  
  loadBook(bookName) {
    let url = this.configService.fetchJournalEntryUrl;
    let requestData = {
    	'cookie' : this.configService.getCookie(),
    	'id' : bookName
    };
    
    let headers = new Headers();
    headers.append("content-type", "application/json");
  
  	return this.http.post(url, requestData, headers);
  }

}
