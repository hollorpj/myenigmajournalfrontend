import { TestBed, inject } from '@angular/core/testing';

import { SettingsEditorHustlerService } from './settings-editor-hustler.service';

describe('SettingsEditorHustlerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SettingsEditorHustlerService]
    });
  });

  it('should be created', inject([SettingsEditorHustlerService], (service: SettingsEditorHustlerService) => {
    expect(service).toBeTruthy();
  }));
});
