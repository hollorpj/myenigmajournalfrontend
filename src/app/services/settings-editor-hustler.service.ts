import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/Rx';

@Injectable()
export class SettingsEditorHustlerService {

  private closeWindow = new BehaviorSubject<boolean>(false);
  isWindowOpen = this.closeWindow.asObservable();
  
  constructor() { 
  
  }
  
  closeTheSettingsWindow() {
  	this.closeWindow.next(false);
  }
  
  openTheSettingsWindow() {
    this.closeWindow.next(true);
  }
  
}
