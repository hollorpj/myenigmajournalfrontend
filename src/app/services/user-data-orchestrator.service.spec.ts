import { TestBed, inject } from '@angular/core/testing';

import { UserDataOrchestratorService } from './user-data-orchestrator.service';

describe('UserDataOrchestratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserDataOrchestratorService]
    });
  });

  it('should be created', inject([UserDataOrchestratorService], (service: UserDataOrchestratorService) => {
    expect(service).toBeTruthy();
  }));
});
