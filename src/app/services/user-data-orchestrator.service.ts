import { Injectable } from '@angular/core';

@Injectable()
export class UserDataOrchestratorService {

  constructor() { }
  
  fetchCurrentSessionCookie() {
    let username = this.getCurrentUser();
    let cookieCandidates = document.cookie.split(";");
    let cookie = undefined;
    cookieCandidates.forEach(function(cookieCandidate) {
      if (cookieCandidate.split("=")[0].trim() === username) {
        cookie = cookieCandidate.trim();
      }
    });
    
    return cookie;
  }
  
  setCurrentUser(username) {
    document.cookie = "SimpleUserName=" + username;
  }
  
  getCurrentUser() {
    let cookies = document.cookie.split(";");
    let curUser = undefined;
    cookies.forEach(function(cookie) {
      if (cookie.split("=")[0].trim() === "SimpleUserName") {
        curUser = cookie.split("=")[1].trim();
        return;
      }
    });
    
    return curUser;
  }

}
