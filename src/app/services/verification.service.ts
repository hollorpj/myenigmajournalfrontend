import { ConfigService } from './configservice.service';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class VerificationService {

  constructor(private configService : ConfigService, private http : Http) { }
  
  isCookieStillAlive() {
    let cookie = document.cookie;
    let verificationUrl = this.configService.getCookieVerificationUrl();
   
    let userId = cookie.split("=")[0];
    let cookieId = cookie.split("=")[1];  
   
    let formData = {
      'userId' : userId,
      'cookieId' : cookieId
    }
    
    let headers = new Headers();
    headers.append('content-type', 'application/json');
    
    this.http.post(verificationUrl, formData, headers).subscribe(response => {
      console.log(response);
    });
  }

}
